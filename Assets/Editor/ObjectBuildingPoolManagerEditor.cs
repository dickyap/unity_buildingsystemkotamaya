﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.IO;
using KotaMaya.BuildingSystem;

[CustomEditor(typeof(ObjectBuildingPoolManager))]
public class ObjectBuildingPoolManagerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        EditorGUILayout.Space();

        if (GUILayout.Button("Get All Objects"))
            GetAll();

        EditorGUILayout.Space();
    }

    void GetAll()
    {
        ObjectBuildingPoolManager myScript = (ObjectBuildingPoolManager)target;
        
        string folderPath = EditorUtility.OpenFolderPanel("Load Objects Folder", "Assets/Datas", "").Replace(Application.dataPath, "Assets/");
        string[] allFiles = AssetDatabase.FindAssets("t:ObjectBuilding", new[] { folderPath });

        myScript.listAllObjects = new List<ObjectBuilding>();

        foreach (var file in allFiles)
            myScript.listAllObjects.Add(AssetDatabase.LoadAssetAtPath<ObjectBuilding>(AssetDatabase.GUIDToAssetPath(file)));
        
        EditorUtility.SetDirty(myScript);
    }
}