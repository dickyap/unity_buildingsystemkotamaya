﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KotaMaya.BuildingSystem
{
    public class ObjectBuildingPoolManager : MonoBehaviour
    {
        public static ObjectBuildingPoolManager init;

        [SerializeField] Transform _parentObjs;
        public List<ObjectBuilding> listAllObjects; // editor needed

        Dictionary<string, ObjectBuilding> _dictAllObjects = new Dictionary<string, ObjectBuilding>();
        Dictionary<string, List<GameObject>> _dictAllObjToPlace = new Dictionary<string, List<GameObject>>();
        Dictionary<string, List<GameObject>> _dictAllObjPreview = new Dictionary<string, List<GameObject>>();

        void Awake()
        {
            init = this;
            InitializeObjectPool();
        }

        void InitializeObjectPool()
        {
            for (int i = 0; i < listAllObjects.Count; i++)
            {
                _dictAllObjects.Add(listAllObjects[i].objectID, listAllObjects[i]);
                GenerateObj(_dictAllObjToPlace, listAllObjects[i].objectID, listAllObjects[i].objectToPlace); // obj to place
                GenerateObj(_dictAllObjPreview, listAllObjects[i].objectID, listAllObjects[i].objectPreview); // obj preview
            }
        }

        GameObject GenerateObj(Dictionary<string, List<GameObject>> targetDict, string key, GameObject obj)
        {
            if (!targetDict.ContainsKey(key))
                targetDict.Add(key, new List<GameObject>());

            GameObject tempObj = Instantiate(obj, _parentObjs);
            tempObj.name = string.Format("{0}_{1}_{2}_{3}",
                _dictAllObjects[key].objectName.Replace("_", ""),
                _dictAllObjects[key].objectTargetLayer,
                _dictAllObjects[key].isScalable ? "scalable" : null,
                _dictAllObjects[key].isSnappable ? "snappable" : null);

            ReturnObject(tempObj);

            foreach (var item in tempObj.GetComponentsInChildren<Renderer>())
                item.material.renderQueue = (_dictAllObjects[key].isMaskable) ? 3002 : item.material.renderQueue;

            targetDict[key].Add(tempObj);
            return tempObj;
        }

        public void ReturnObject(GameObject objToReturn)
        {
            objToReturn.transform.SetParent(_parentObjs);
            objToReturn.transform.localEulerAngles = objToReturn.transform.localPosition = Vector3.zero;
            objToReturn.transform.localScale = objToReturn.transform.GetChild(0).localScale = Vector3.one;
            objToReturn.SetActive(false);
        }

        public GameObject GetObjectPreview(string objID) => GetObject(objID, _dictAllObjPreview);
        public GameObject GetObjectToPlace(string objID) => GetObject(objID, _dictAllObjToPlace);
        public Color GetObjectBaseColor(string objID) => _dictAllObjects[objID].objectBaseColor;

        GameObject GetObject(string objID, Dictionary<string, List<GameObject>> fromDict)
        {
            foreach (var item in fromDict[objID])
                if (!item.activeSelf) return item;

            return GenerateObj(fromDict, objID, fromDict[objID][0]);
        }
    }
}
