﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using KotaMaya.General;
using KotaMaya.Effect;

namespace KotaMaya.BuildingSystem
{
    public class BuildingSystemManager : MonoBehaviour
    {
        [Header("Mode Building")]
        [SerializeField] Animator _animModeBtn;
        [SerializeField] Animator _animSubModeBtn;

        GameObject _objectToBuild, _objectToDestroy, _objToColor;

        enum BuildingMode { build, destroy, coloring }
        BuildingMode _currentBuildingMode = 0;

        bool _isObjPlaced, _isObjBuilt, _isObjReadyToScale, _isObjScalable, _isObjSnappable;
        string _currentObjectID, _currentObjectTargetLayer;

        Color _initialSelectedColor = Color.clear, _currentTargetColor = new Color(0.96f, 0.96f, 0.92f, 1);

        void BuildModeController()
        {
            CursorManager.targetCursorMode = ConfigGeneral.CURSOR_BUILD;
            if (_objectToBuild == null) return;

            Vector3Int roundToIntPos = Vector3Int.RoundToInt(MouseRaycaster.GetHitObjectPosition(LayerMask.GetMask(ConfigGeneral.LayerMask.FLOOR, _currentObjectTargetLayer)));
            Vector3Int roundToInEuler = Vector3Int.RoundToInt((_isObjSnappable) ? MouseRaycaster.GetHitObjectLocalEuler(LayerMask.GetMask(_currentObjectTargetLayer)) : MouseRaycaster.outOfBoundValue);
            bool isNeedFacingCursor = !_isObjScalable && (Input.GetMouseButton(0) || Input.GetMouseButtonUp(0)) || (_isObjPlaced && _isObjScalable);

            _isObjPlaced = (!EventSystem.current.IsPointerOverGameObject() && Input.GetMouseButtonDown(0)) ? (_isObjSnappable && !MouseRaycaster.IsHitObjectTargetLayer(LayerMask.GetMask(_currentObjectTargetLayer))) ? false : true : _isObjPlaced;
            _objectToBuild.transform.position = (!_isObjPlaced) ? Vector3.Lerp(_objectToBuild.transform.position, roundToIntPos, Time.deltaTime * 25) : Vector3Int.RoundToInt(_objectToBuild.transform.position);
            
            _objectToBuild.transform.localEulerAngles = 
                (roundToInEuler != MouseRaycaster.outOfBoundValue && _isObjSnappable) ? roundToInEuler + (Vector3.up * 90) :
                    (_isObjScalable && !_isObjReadyToScale) ? _objectToBuild.transform.localEulerAngles : 
                        RotateFacing((isNeedFacingCursor) ? roundToIntPos - _objectToBuild.transform.position : 
                            Camera.main.transform.position - _objectToBuild.transform.position);

            _objectToBuild.transform.GetChild(0).localScale = ScaleObjectAnchor((_isObjReadyToScale && _isObjScalable) ? roundToIntPos : MouseRaycaster.outOfBoundValue);

            if (Input.GetMouseButtonUp(0) && !EventSystem.current.IsPointerOverGameObject() && _isObjPlaced)
            {
                _objectToBuild =  (_isObjScalable) ? _isObjReadyToScale ? ReselectObject() : _objectToBuild : null;
                _isObjReadyToScale = _isObjPlaced = _isObjPlaced && _isObjScalable;
            }

            if (Input.GetKeyUp(KeyCode.Escape)) ClearObject();
        }

        Vector3 RotateFacing(Vector3 targetRotRaw) => Quaternion.Slerp(_objectToBuild.transform.rotation, Quaternion.AngleAxis(Mathf.Round(Quaternion.LookRotation(targetRotRaw).eulerAngles.y / 45f) * 45f, Vector3.up), Time.deltaTime * 25).eulerAngles;

        Vector3 ScaleObjectAnchor(Vector3 roundToIntPos)
        {
            if (roundToIntPos == MouseRaycaster.outOfBoundValue)
                return _objectToBuild.transform.GetChild(0).localScale;

            Vector3 targetScale = _objectToBuild.transform.GetChild(0).localScale;
            targetScale.z = Mathf.Lerp(targetScale.z, Mathf.Round((Vector3.Distance(_objectToBuild.transform.position, roundToIntPos) * 2) + (1f)), Time.deltaTime * 20);

            _objectToBuild.GetComponentInChildren<Renderer>().material.SetTextureScale("_MainTex", new Vector3(.25f * targetScale.z, 1));
            return targetScale;
        }

        GameObject ReselectObject()
        {
            CameraShake.init.Shake();
            SelectObject(_currentObjectID);
            return _objectToBuild;
        }

        void HighlightObject(Color targetColor, ref GameObject targetObj, out Transform targetParent, LayerMask targetLayerMask)
        {
            GameObject tempHitObj = MouseRaycaster.GetHitObject(targetLayerMask);
            targetParent = (targetObj != null) ? targetObj.GetComponentInParent<TagParent>().SelectParent().transform : null;
            bool isOnEnter = tempHitObj != null && targetObj != tempHitObj, isOnExit = ((tempHitObj == null || targetObj != tempHitObj) && targetObj != null);

            SetColors((isOnEnter && _initialSelectedColor != Color.clear && targetObj != null) ? targetParent : null, _initialSelectedColor);

            targetObj = (isOnEnter) ? tempHitObj : (isOnExit) ? null : targetObj;
            _initialSelectedColor = (isOnEnter) ? targetObj.GetComponentInChildren<Renderer>().material.color : _initialSelectedColor;
            targetParent = (isOnEnter) ? targetObj.GetComponentInParent<TagParent>().SelectParent().transform : targetParent;

            SetColors(targetParent, (isOnEnter) ? targetColor : (isOnExit) ? _initialSelectedColor : Color.clear);
        }

        void DestroyModeController()
        {
            CursorManager.targetCursorMode = ConfigGeneral.CURSOR_DEST;
            HighlightObject(Color.red, ref _objectToDestroy, out Transform parentObj, LayerMask.GetMask(ConfigGeneral.LayerMask.WALL, ConfigGeneral.LayerMask.DESTROYABLE));

            if (Input.GetMouseButtonUp(0) && _objectToDestroy != null)
            {
                SetColors(parentObj, _initialSelectedColor);
                ObjectBuildingPoolManager.init.ReturnObject(parentObj.gameObject);
                ClearObject();
            }
        }

        void ColoringModeController()
        {
            CursorManager.targetCursorMode = ConfigGeneral.CURSOR_COL;
            HighlightObject(_currentTargetColor, ref _objToColor, out Transform parentObj, LayerMask.GetMask(ConfigGeneral.LayerMask.WALL, ConfigGeneral.LayerMask.DESTROYABLE));

            if (Input.GetMouseButtonUp(0) && _objToColor != null)
            {
                _initialSelectedColor = _currentTargetColor;
                SetColors(parentObj, _currentTargetColor);
            }
        }

        void SetColors(Transform parent, Color color)
        {
            if (parent == null || color == Color.clear) return;

            foreach (var item in parent.GetComponentsInChildren<Renderer>())
                item.material.color = color;
        }

        void ClearObject()
        {
            if (_objectToBuild != null)
                ObjectBuildingPoolManager.init.ReturnObject(_objectToBuild);

            _objectToBuild = _objectToDestroy = _objToColor = null;
            _initialSelectedColor = Color.clear;

            _currentObjectID = (_isObjReadyToScale) ? _currentObjectID : null;

            if (_isObjReadyToScale) SelectObject(_currentObjectID);

            _isObjReadyToScale = _isObjPlaced = false;
        }

        void Update()
        {
            switch (_currentBuildingMode)
            {
                case BuildingMode.build: BuildModeController(); break;
                case BuildingMode.destroy: DestroyModeController(); break;
                case BuildingMode.coloring: ColoringModeController(); break;
            }
        }

        void ChangeBuildingMode(BuildingMode selectedMode)
        {
            _currentBuildingMode = selectedMode;
            _animModeBtn.SetTrigger((selectedMode == BuildingMode.coloring) ? ConfigGeneral.MODE_BTN_COL_TRIGGER : (selectedMode == BuildingMode.destroy) ? ConfigGeneral.MODE_BTN_DEST_TRIGGER : ConfigGeneral.MODE_BTN_BUILD_TRIGGER);
            ClearObject();
        }

        // 0 == EXT; 1 == INT
        public void SetSubMode(int idx) => _animSubModeBtn.SetTrigger((idx == 0) ? ConfigGeneral.SUBMODE_BTN_EXT_TRIGGER : ConfigGeneral.SUBMODE_BTN_INT_TRIGGER);
        public void SetBuildMode() => ChangeBuildingMode(BuildingMode.build);
        public void SetDestroyMode() => ChangeBuildingMode(BuildingMode.destroy);
        public void SetColoringMode() => ChangeBuildingMode(BuildingMode.coloring);
        public void SelectColor(UnityEngine.UI.Image sourceColor) => _currentTargetColor = sourceColor.color;

        public void SelectObject(string objID)
        {
            _currentObjectID = objID;

            if (!_isObjPlaced && _objectToBuild != null) ObjectBuildingPoolManager.init.ReturnObject(_objectToBuild);

            _objectToBuild = ObjectBuildingPoolManager.init.GetObjectToPlace(objID);
            SetColors(_objectToBuild.transform, ObjectBuildingPoolManager.init.GetObjectBaseColor(objID));
            _objectToBuild.SetActive(true);

            _currentObjectTargetLayer = _objectToBuild.name.Split('_')[1];
            _isObjScalable = !string.IsNullOrEmpty(_objectToBuild.name.Split('_')[2]);
            _isObjSnappable = !string.IsNullOrEmpty(_objectToBuild.name.Split('_')[3]);
            _isObjPlaced = _isObjReadyToScale = false;
        }
    }
}
