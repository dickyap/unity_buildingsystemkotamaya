﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using KotaMaya.BuildingSystem;

namespace KotaMaya.General
{
    public class CursorManager : MonoBehaviour
    {
        [SerializeField] Texture2D[] _allCursors;
        public static string targetCursorMode;
        string _currentCursorMode;
        Dictionary<string, Texture2D> _dictCursor = new Dictionary<string, Texture2D>();

        private void Awake()
        {
            foreach (var item in _allCursors) 
                _dictCursor.Add(item.name, item);
        }

        void LateUpdate() => SetCursor((EventSystem.current.IsPointerOverGameObject()) ? ConfigGeneral.CURSOR_MAIN : (Input.GetMouseButton(1)) ? ConfigGeneral.CURSOR_ROT : (Input.GetMouseButton(2))? ConfigGeneral.CURSOR_MOVE : targetCursorMode);
        
        void SetCursor(string CursorID) => Cursor.SetCursor(_dictCursor[CursorID], new Vector2(6, 8), CursorMode.Auto);
    }
}

