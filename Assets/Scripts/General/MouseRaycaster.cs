﻿using UnityEngine;

namespace KotaMaya.General
{
    public class MouseRaycaster
    {
        public static Vector3 outOfBoundValue = Vector3.down * 50;

        static Ray ScreenPointToRay() => Camera.main.ScreenPointToRay(Input.mousePosition);
        public static GameObject GetHitObject(LayerMask layerMask) => (Physics.Raycast(ScreenPointToRay(), out RaycastHit hit, Mathf.Infinity, layerMask)) ? hit.transform.gameObject : null;
        public static bool IsHitObjectTargetLayer(LayerMask layerMask) => Physics.Raycast(ScreenPointToRay(), out RaycastHit hit, Mathf.Infinity, layerMask);
        public static Vector3 GetHitObjectPosition(LayerMask layerMask) => (Physics.Raycast(ScreenPointToRay(), out RaycastHit hit, Mathf.Infinity, layerMask)) ? hit.point : outOfBoundValue;
        public static Vector3 GetHitObjectLocalEuler(LayerMask layerMask)
        {
            Transform hitTransform = (Physics.Raycast(ScreenPointToRay(), out RaycastHit hit, Mathf.Infinity, layerMask)) ? hit.transform.GetComponentInParent<TagParent>() != null ? hit.transform.GetComponentInParent<TagParent>().SelectParent().transform : hit.transform : null;
            return hitTransform != null ? hitTransform.localEulerAngles : outOfBoundValue;
        }
    }
}

