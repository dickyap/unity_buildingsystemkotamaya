﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KotaMaya.Effect
{
    public class CameraShake : MonoBehaviour
    {
        public static CameraShake init;
        [SerializeField] Camera _cam;
        float _duration = .15f, _speed = 15f, _power = 7.5f, _time, _lastFoV, _nextFoV;
        AnimationCurve _curve = AnimationCurve.EaseInOut(0, 1, 1, 0);
        
        void Awake() => init = this;
        public void Shake() => _time = _duration;

        void LateUpdate()
        {
            _time -= (_time > 0) ? Time.deltaTime : _time;

            _nextFoV = (_time > 0) ? (Mathf.PerlinNoise(_time * _speed * 2, _time * _speed * 2) - 0.5f) * _power * _curve.Evaluate(1f - _time / _duration) : 0;
            _cam.fieldOfView += (_time > 0) ? (_nextFoV - _lastFoV) : -_lastFoV;
            _lastFoV = (_time > 0) ? _nextFoV : 0;
        }
    }
}
