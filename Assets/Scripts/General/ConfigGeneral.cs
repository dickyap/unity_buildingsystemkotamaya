﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KotaMaya.General
{
    public class ConfigGeneral : MonoBehaviour
    {
        [System.Serializable]
        public struct LayerMask
        {
            public const string FLOOR = "Floor", WALL = "Wall", DESTROYABLE = "Destroyable";
        }

        public const string MODE_BTN_BUILD_TRIGGER = "ModeBtnBuilding-Build", MODE_BTN_DEST_TRIGGER = "ModeBtnBuilding-Destroy", MODE_BTN_COL_TRIGGER = "ModeBtnBuilding-Color", SUBMODE_BTN_EXT_TRIGGER = "SubModeBtnBuilding-Exterior", SUBMODE_BTN_INT_TRIGGER = "SubModeBtnBuilding-Interior";
        public const string CURSOR_MAIN = "cursor-main", CURSOR_BUILD = "cursor-build", CURSOR_DEST = "cursor-destroy", CURSOR_MOVE = "cursor-move", CURSOR_COL = "cursor-color", CURSOR_ROT = "cursor-rotate";
    }
}