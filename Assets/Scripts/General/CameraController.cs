﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace KotaMaya.General
{
    public class CameraController : MonoBehaviour
    {
        [SerializeField] Transform _cameraAnchor;

        Vector2 _mousePosStart, _mouseRotStart, _rotVelocity;
        float _rotateSensitivity = .2f, _multiplier = 1;
        bool _isRotating, _isRotatable, _isMoving, _isMovable, _isZoomable;

        void RotateCam(Vector2 pos)
        {
            if (pos == Vector2.zero) return;

            _mouseRotStart = (!_isRotating) ? pos : _mouseRotStart;
            _isRotating = true;

            Vector2 _mouseOffset = (pos - _mouseRotStart);
            _rotVelocity = new Vector2((_mouseOffset.y) * _rotateSensitivity, (_mouseOffset.x) * _rotateSensitivity) * (Time.deltaTime * 25f);

            _cameraAnchor.Rotate(-_rotVelocity.x, _rotVelocity.y, 0, Space.Self);

            _cameraAnchor.localEulerAngles = new Vector3(ClampAngle(_cameraAnchor.localEulerAngles.x, 0, 60), _cameraAnchor.localEulerAngles.y, 0);
            _mouseRotStart = pos;
        }

        float ClampAngle(float angle, float from, float to)
        {
            angle = (angle < 0f) ? 360 + angle : angle;
            return (angle > 180f) ? Mathf.Max(angle, 360 + from) : Mathf.Min(angle, to);
        }

        void MoveCam(Vector2 pos)
        {
            if (pos == Vector2.zero) return;

            _mousePosStart = (!_isMoving) ? pos : _mousePosStart;
            _isMoving = true;

            Vector2 speedMove = new Vector2(-(_mousePosStart.x - pos.x) / Screen.width, -(_mousePosStart.y - pos.y) / Screen.height);
            Vector3 desiredDirection = Vector3.Normalize(new Vector3(_cameraAnchor.forward.x, 0, _cameraAnchor.forward.z)) * speedMove.y + Vector3.Normalize(new Vector3(_cameraAnchor.right.x, 0, _cameraAnchor.right.z)) * speedMove.x;

            _cameraAnchor.position += desiredDirection * Time.deltaTime * (_multiplier * 25f);
            _cameraAnchor.position = new Vector3(Mathf.Clamp(_cameraAnchor.position.x, -50, 50), _cameraAnchor.position.y, Mathf.Clamp(_cameraAnchor.position.z, -50, 50));
        }

        void ZoomCam(float pos)
        {
            Vector3 newPos = _cameraAnchor.GetChild(0).localPosition;
            newPos.z = Mathf.Clamp(newPos.z + ((_multiplier * -pos) * Time.deltaTime * 20), -70, -10);
            _cameraAnchor.GetChild(0).localPosition = newPos;
        }

        void LateUpdate()
        {
            _isRotatable = Input.GetMouseButton(1) && !EventSystem.current.IsPointerOverGameObject();
            _isMovable = Input.GetMouseButton(2) || (Input.GetAxis("Horizontal") != 0f || Input.GetAxis("Vertical") != 0f);
            _isZoomable = Input.mouseScrollDelta.y != 0;
            _multiplier = Input.GetKey(KeyCode.LeftShift) ? 2 : 1;

            RotateCam((_isRotatable) ? (Vector2)Input.mousePosition : Vector2.zero);
            _isRotating = !_isRotatable ? false : _isRotating;

            MoveCam(_isMovable ? (Input.GetMouseButton(2)) ? (Vector2)Input.mousePosition : new Vector2(Input.GetAxis("Horizontal") * Screen.width, Input.GetAxis("Vertical") * Screen.height) : Vector2.zero);
            _isMoving = !_isMovable ? false : _isMoving;

            ZoomCam((_isZoomable) ? Input.mouseScrollDelta.y : 0);
        }
    }
}