﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KotaMaya.BuildingSystem
{
    [System.Serializable]
    [CreateAssetMenu(fileName = "ObjectBuilding", menuName = "Kota Maya/New Object Building SO", order = 1)]
    public class ObjectBuilding : ScriptableObject
    {   
        public string objectID, objectName;
        public bool isMaskable, isScalable, isSnappable;
        public ObjectTargetLayer objectTargetLayer;
        public Sprite objectThumbnail;
        public GameObject objectPreview, objectToPlace;
        public Color objectBaseColor;

        public enum ObjectTargetLayer
        {
            Floor, Wall, All
        }
    }
}